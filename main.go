package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
)

// Handles SIGINTS signals so that when the program closes it won't display an error
func handleInterrupt() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
	<-sig
	fmt.Println("Exiting...")
	os.Exit(0)
}

func main() {
	go handleInterrupt()
	port := ":4444"
	udpServer, err := net.ListenPacket("udp", port)
	if err != nil {
		log.Fatalln("Could not start listenting on port: " + port + ". " + err.Error())
	}
	defer udpServer.Close()

	log.Println("Serving on port " + port)

	for {
		buffer := make([]byte, 2048)
		_, addr, err := udpServer.ReadFrom(buffer)
		if err != nil {
			log.Println("An error ocurred: " + err.Error())
		}
		log.Println("Address: " + addr.String() + " Msg: " + string(buffer[:]))
	}
}
